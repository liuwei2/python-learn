# coding=utf-8
from __future__ import division


def vector_add(v, w):
    """adds corresponding elements"""
    return tuple([v_i + w_i for v_i, w_i in zip(v, w)])


def vector_subtract(v, w):
    """subtracts corresponding elements"""
    return tuple([v_i - w_i for v_i, w_i in zip(v, w)])


def vector_sum(vectors):
    return reduce(vector_add, vectors)


def scalar_multiply(c, v):
    """c is a number, v is a vector"""
    return tuple([c * v_i for v_i in v])


def scalar_division(c, v):
    """c is a number, v is a vector"""
    return tuple([v_i / c for v_i in v])


def vector_mean(vectors):
    """compute the vector whose ith element is the mean of the
    ith elements of the input vectors"""
    n = len(vectors)
    return scalar_division(n, vector_sum(vectors))

def vector_dot(v, w):
    """v_1 * w_1 + ... + v_n * w_n"""
    return sum(v_i * w_i for v_i, w_i in zip(v, w))

if __name__ == '__main__':
    assert vector_add((1, 2), (3, 4)) == (4, 6)
    assert vector_subtract((3, 4), (1, 2)) == (2, 2)
    assert vector_sum([(1, 2), (3, 4), (5, 6)]) == (9, 12)
    assert scalar_multiply(2, (1, 2)) == (2, 4)
    assert vector_mean([(2, 2), (3, 4), (5, 6)]) == (10 / 3, 4)
    assert vector_dot((1,2),(3,4))==11
