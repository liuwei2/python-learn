# coding=utf-8
import math
import numpy as np
import tensorflow as tf
import theano.tensor as T

def fib(n):    # write Fibonacci series up to n
    """Print a Fibonacci series up to n."""
    a, b = 0, 1
    t = 0
    while t < n:
        t += 1
        a, b = b, a + b


def sigmoid(x):
    return 1.0 / (1.0 + np.exp(-x))


def softmax(A):
    expA = np.exp(A)
    return expA / expA.sum(axis=1, keepdims=True)


def y2indicator(y, n):
    '''
    如果y不是0或者1，比如说数字识别，就需要输出10个数字用来标识是哪个数字
    n代表需要多少种类型输出结果
    '''
    y_len = len(y)
    ind = np.zeros((y_len, n))
    for i in xrange(y_len):
        ind[i, y[i]] = 1
    return ind


class Test:

    def __getitem__(self, index):
        return index
# 是否肥胖的数据
# 特征有 身高(cm) 体重(kg)
# inputs is a 5*2 matrix,5 is number of samples,2 is number of features
inputs = np.array([[170, 60], [142, 42], [158, 60], [188, 80], [175, 65]])
labels = np.array([[0], [1], [1], [0], [0]])
# M is number of hidden units
# W = np.random.randn(D, M)
#
# V = np.random.randn(M, K)
test = Test();
test[1:1,[1,2]]
