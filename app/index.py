#!/usr/bin/python
# coding=utf-8
from matplotlib import pyplot as plt


'''
|5x-6|<e
|5(x-1)-1|<e
5|(x-1)-1/5|<e
'''
def foo(x):
    '''
    f(x)=x+1
    '''
    return x + 1


def foo2(x):
    '''
    令g=2x
    f(2x)=f(g)=g+1=2x+1
    '''
    return 2 * x + 1

xs = range(100)
ys = [foo(x) for x in xs]
ys2 =  [foo2(x) for x in xs]
fig = plt.figure()
ax1 = fig.add_subplot(1,1,1,xlim=(0, 300), ylim=(0,300))
ax2 = fig.add_subplot(1,1,1,xlim=(0, 300), ylim=(0, 300))
ax1.plot(xs, ys, color='green', linestyle='solid')
ax2.plot(xs, ys2, color='red', linestyle='solid')
plt.show()
