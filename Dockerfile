FROM registry.aliyuncs.com/chuangjike/debian
RUN apt-get update -y
RUN apt-get install -y python-matplotlib python-tk python-pip
RUN pip install requests
